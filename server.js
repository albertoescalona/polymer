var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
app.use(express.static(__dirname + '/build/default'));
app.listen(port);

console.log('Esperando que funcione Polymer en node: ' + port);

 app.get('/', function(req, res){
  //res.send("Hola Mundo NodeJS");
  res.sendFile("index.html", {root: '.'});
  //dirname en la raiz de donde estas parado.
});
